#Include "rwmake.ch"
#Include "topconn.ch"

//SRSF 15/11/2012 - Re-senquenciar matricula dos funcionarios

User Function ZSeqFunc()
************************
Local cQry  := ""
Local nRegs := 0 
Local nMat  := 0

cQry := "Select RA_MAT, RA_MATRM From SRA010 Where D_E_L_E_T_ = ' ' Order by RA_ADMISSA, RA_MAT "
If Select("QRYSRA") > 0 
   QRYSRA->(DbCloseArea())
EndIf

TcQuery cQry New Alias "QRYSRA"  
nRegs := Contar("QRYSRA","! Eof()")
DbGoTop()

ProcRegua(nRegs)

Begin Transaction

While ! QRYSRA->(Eof()) 
    IncProc("Matricula: "+QRYSRA->RA_MAT+" ...")

    nMat++          
//    cQry := "Update RHK010 Set RHK_MAT = '"+QRYSRA->RA_MAT+"' Where D_E_L_E_T_ = ' ' And RHK_MAT = '"+QRYSRA->RA_MATRM+"' "   
//    TcSqlExec(cQry)

//    cQry := "Update RHL010 Set RHL_MAT = '"+QRYSRA->RA_MAT+"' Where D_E_L_E_T_ = ' ' And RHL_MAT = '"+QRYSRA->RA_MATRM+"' "   
//    TcSqlExec(cQry)

    cQry := "Update SRB010 Set RB_MAT = '"+QRYSRA->RA_MAT+"' Where D_E_L_E_T_ = ' ' And RB_MAT = '"+QRYSRA->RA_MATRM+"' "   
    TcSqlExec(cQry)
    
/*
    cQry := "Update SRA010 Set RA_MAT = '"+StrZero(nMat,6)+"' Where D_E_L_E_T_ = ' ' And RA_MAT = '"+QRYSRA->RA_MAT+"' "   
    TcSqlExec(cQry)
    cQry := "Update SR0010 Set R0_MAT = '"+StrZero(nMat,6)+"' Where D_E_L_E_T_ = ' ' And R0_MAT = '"+QRYSRA->RA_MAT+"' "
    TcSqlExec(cQry)
    cQry := "Update SR3010 Set R3_MAT = '"+StrZero(nMat,6)+"' Where D_E_L_E_T_ = ' ' And R3_MAT = '"+QRYSRA->RA_MAT+"' "
    TcSqlExec(cQry)
    cQry := "Update SR7010 Set R7_MAT = '"+StrZero(nMat,6)+"' Where D_E_L_E_T_ = ' ' And R7_MAT = '"+QRYSRA->RA_MAT+"' "
    TcSqlExec(cQry)
    cQry := "Update SRC010 Set RC_MAT = '"+StrZero(nMat,6)+"' Where D_E_L_E_T_ = ' ' And RC_MAT = '"+QRYSRA->RA_MAT+"' "
    TcSqlExec(cQry)
    cQry := "Update SR9010 Set R9_MAT = '"+StrZero(nMat,6)+"' Where D_E_L_E_T_ = ' ' And R9_MAT = '"+QRYSRA->RA_MAT+"' "
    TcSqlExec(cQry)
    cQry := "Update SRD010 Set RD_MAT = '"+StrZero(nMat,6)+"' Where D_E_L_E_T_ = ' ' And RD_MAT = '"+QRYSRA->RA_MAT+"' "
    TcSqlExec(cQry)
    cQry := "Update SRE010 Set RE_MAT = '"+StrZero(nMat,6)+"' Where D_E_L_E_T_ = ' ' And RE_MAT = '"+QRYSRA->RA_MAT+"' "
    TcSqlExec(cQry)
    cQry := "Update SRF010 Set RF_MAT = '"+StrZero(nMat,6)+"' Where D_E_L_E_T_ = ' ' And RF_MAT = '"+QRYSRA->RA_MAT+"' "
    TcSqlExec(cQry)
*/

    QRYSRA->(DbSkip())
    
EndDo

End Transaction

QRYSRA->(DbCloseArea())
   
MsgInfo("Processo Finalizado","Aten��o")

Return