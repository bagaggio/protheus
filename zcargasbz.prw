#Include "Rwmake.ch"
#include "PROTHEUS.ch" 
#Include "TOPCONN.CH"

User Function zcargasbz()

Local cItem := 0

DbSelectArea("SBZ")
DbSetOrder(1)

cQuery := "	SELECT  SB1.B1_COD COD,"+;
		  "	        SB1.B1_LOCPAD LOCPAD,"+;
		  "	        SB1.B1_GRTRIB GRTRIB,"+;		  
		  "	        SB1.B1_ORIGEM ORIGEM,"+;
		  "	        SB1.B1_IPI IPI,"+;		  
		  "	        SB1.B1_PICM PICM"+;
		  "	FROM SB1010 SB1 "+;
		  "	WHERE SB1.B1_FILIAL='"+xFilial("SB1")+"' AND "+; 
		  "	      SB1.D_E_L_E_T_=' ' "                 

TcQuery cQuery NEW ALIAS "QRY"

QRY->(DbGoTop())

ProcRegua(RecCount())

While !EOF()                  
	    DbSelectArea("SBZ") 
	    If !DbSeek(xFilial("SBZ")+QRY->COD)
		    RecLock("SBZ",.t.)
		    SBZ->BZ_FILIAL   := xFilial("SBZ")
		   	SBZ->BZ_COD      := QRY->COD      
		   	SBZ->BZ_LOCPAD   := QRY->LOCPAD
		   	SBZ->BZ_GRTRIB   := QRY->GRTRIB
		   	SBZ->BZ_ORIGEM   := QRY->ORIGEM
		   	SBZ->BZ_IPI      := QRY->IPI
		   	SBZ->BZ_PICM     := QRY->PICM
		    MsUnlock()
		    cItem++ 
	    EndIf

    DbSelectArea("QRY")
    QRY->(DbSkip())
    IncProc()
End

QRY->(DbcloseArea())                                                                       

Msginfo('Foram gerados '+str(cItem)+' registros de indicadores de produto !!!')

Return()