#Include "TopConn.Ch"
#Include "Protheus.Ch"
#Include "RwMake.Ch"

*************************
User Function BAGINVENT()
*************************

 Local cQuery    := ""
 Local cProduto  := ""
 Local cAlmOri   := ""
 Local cAlmDes   := """
 Local xAlias    := GetNextAlias()
 Local nQtdTrans := 0
 Local aItensNew := {}
 
 Private lMsHelpAuto := .T.
 Private lMsErroAuto := .F.
 
 cQuery := ""
 cQuery += " Select	CONVERT(VARCHAR(15),REPLACE(A.CODIGO,'''',''))		PRODUTO		,       "
 cQuery += " 		A.DESCRICAO											DESCRICAO	,       "
 cQuery += " 		CONVERT(FLOAT,[QTD#INV])							QTDINV		,       "
 cQuery += " 		CONVERT(FLOAT,A.SALDO)								SALDO		,       "
 cQuery += " 		CONVERT(FLOAT,A.DIFERENCA)							DIFERENCA	,       "
 cQuery += " 		CONVERT(FLOAT,[DIF#VALOR])							VALOR               "
 cQuery += " From OpenRowSet('Microsoft.ACE.OLEDB.12.0',                                    "
 cQuery += "     'Excel 12.0 Xml;HDR=YES;Database=E:\Zeus\Leonardo\Estoque\Inventario.xlsx',"
 cQuery += "     'SELECT * FROM [Inventario$]') A                                           "
 cQuery += " ORDER BY CONVERT(VARCHAR(15),REPLACE(A.CODIGO,'''',''))                        "
 
 If Select(xAlias) <> 0 ; (xAlias)->(DbCloseArea()) ; EndIf   
 DbUseArea(.T., "TOPCONN", TCGenQry(,,cQuery), xAlias, .F., .F.) 

 (xAlias)->(DbGoTop())
 While (xAlias)->(!Eof())

    If (xAlias)->DIFERENCA == 0 
       (xAlias)->(DbSkip())
       Loop
    EndIf
    
    cProduto  := Alltrim((xAlias)->PRODUTO))
    SB1->(DbSetOrder(1))
    If !SB1->(DbSeek(xFilial("SB1")+cProduto))
       Aviso("Aten��o","Produto "+cProduto+" n�o encontrado no cadastro",{"Ok"})
       Exit
    EndIf

    cAlmOri   := If( (xAlias)->DIFERENCA > 0 , "50" , "01")
    cAlmDes   := If( (xAlias)->DIFERENCA > 0 , "01" , "50")
    cNumSeq   := ProxNum()
    nQtdTrans := (xAlias)->DIFERENCA * If( (xAlias)->DIFERENCA < 0 , (-1) , (1) )
        
    SB2->(DbSetOrder(1)) ; If SB2->(!DbSeek(xFilial("SB2")+cProduto+cAlmOri)) ; CriaSB2(SB1->B1_COD,cAlmOri) ; EndIf
    SB2->(DbSetOrder(1)) ; If SB2->(!DbSeek(xFilial("SB2")+cProduto+cAlmDes)) ; CriaSB2(SB1->B1_COD,cAlmDes) ; EndIf
    
    Aadd(aItensNew,{SB1->B1_COD           ,;
                    SB1->B1_DESC          ,;
                    SB1->B1_UM            ,;
                    cAlmOri               ,;
                    CriaVar("D3_LOCALIZ") ,;
                    SB1->B1_COD           ,;
                    SB1->B1_DESC          ,;
                    SB1->B1_UM            ,;
                    cAlmDes               ,;
                    CriaVar("D3_LOCALIZ") ,;
                    CriaVar("D3_NUMSERI") ,;
                    CriaVar("D3_LOTECTL") ,;
                    CriaVar("D3_NUMLOTE") ,;
                    CriaVar("D3_DTVALID") ,;
                    CriaVar("D3_POTENCI") ,;
                    nQtdTrans             ,;
                    CriaVar("D3_QTSEGUM") ,;
                    CriaVar("D3_ESTORNO") ,;
                    cNumSeq               ,;
                    CriaVar("D3_LOTECTL") ,;
                    CriaVar("D3_DTVALID") ,;
                    CriaVar("D3_ITEMGRD")})
 
    (xAlias)->(DbSkip())
 End

 MsExecAuto({|x,y| MATA261(x,y)},aItensNew,3)
 If lMsErroAuto
    MostraErro()
 EndIf
 
Return(.T.) 