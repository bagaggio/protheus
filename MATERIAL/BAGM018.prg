/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � PRENOTA� Autor � Ronaldo Zeus            � Data � 29/10/10 ���
�������������������������������������������������������������������������͹��  `
���Descricao � Leitura e Importacao Arquivo XML para gera��o de Pre-Nota  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Todas                                                      ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                                                                                           

#INCLUDE "PROTHEUS.CH"         
#include "RWMAKE.ch"
#include "Colors.ch"
#include "Font.ch"
#Include "HBUTTON.CH"
#include "Topconn.ch"  
#Include "TbiConn.Ch"
#Include "Fileio.ch"
#INCLUDE "XMLXFUN.CH"

User Function BAGM018A()
************************

//U_BAGM018('000020344 3',1,)
U_BAGM018(,1,)
Inkey(10)
U_BAGM019(,1,)

Return(.T.)
                                 //c_Orig -> qq diferente de Nil, preparo o ambiente.
User Function BAGM018(c_Param,c_Orig,c_Ped1)
********************************************
Local cFile 		:= Space(10)

Private cPathPri 	:= "\ZNfexml\" 
Private cPathXml 	:= "C:\totvs11" 
Private aFields     := {}
Private cArq
Private aFields2    := {}
Private cArq2                                            	
Private a_CodEmp    := {}
Private cArqLog     := ""
Private nHdlLog     := 0
Private lErro       := .F.
Private n_QtdNF     := 0
Private c_Ped       := IIf(c_Ped1==Nil,Nil,c_Ped1)
      
If c_Orig <> Nil
    Prepare Environment Empresa "01" Filial "010101"
Endif      

If Select("ARQXML") > 0
   ARQXML->(DbCloseArea())
EndIf           

//__cInterNet := Nil           

cEmpAux := cEmpAnt
cFilAux := cFilAnt 

fGrvLog("Inicio da Importacao de XML ")   

dbUseArea(.T.,"TOPCONN","FSNFE","ARQXML",.T.,.F.)

Dbselectarea("ARQXML")                                            
DbClearFil() 

If c_Param <> Nil
	//c_Filtro := 'IMPORTADO_ERP <> "S" .AND. XML_TAM <= 65535 .AND. NUMERO = "'+c_Param+'" '      
	c_Filtro := 'IMPORTADO_ERP <> "S" .AND. NUMERO = "'+c_Param+'" '      
Else	
	c_Filtro := 'IMPORTADO_ERP <> "S" '      
//	c_Filtro := 'IMPORTADO_ERP <> "S" .AND. XML_TAM <= 65535  '      
Endif	

c_Index  := Criatrab(Nil,.f.)     
c_Key    := "CNPJ+NUMERO" 
IndRegua("ARQXML",c_Index,c_Key,,c_Filtro,"Selecionando Registro...")

Dbselectarea("ARQXML")
DbgoTop()
While ARQXML->(!EOF())                                   

    If Upper(Left(ARQXML->RAZAO_SOCIAL,4)) $ "RADU|RIO |TONE|RODR|COM |VILA|CENT|CLAS|J.C.|ERIC|UP I|INDU|FOCC|NOVA|BENN|L N |BOLS|S M |BMCG|ABR |DETT|SAMS|EURO|A G |FERN|"
	   SA1->(Dbsetorder(3))
	   IF SA1->(Dbseek(xFilial("SA1")+ARQXML->CNPJ))	                                   
		    cFile     := ARQXML->XML_NFE
		    c_FilLoja := SA1->A1_01IDLOJ      
		    If (c_FilLoja >= '010101' .and. c_FilLoja <= '011499') .AND. Upper(Left(ARQXML->RAZAO_SOCIAL,4)) <> "RADU
				F_ImpXml(cFile,c_FilLoja)                           			
				n_QtdNF += 1 			                                                                             
		    ElseIf (c_FilLoja >= '011501' .and. c_FilLoja <= '019999') 
				F_ImpXml(cFile,c_FilLoja)                           			
				n_QtdNF += 1 			                                                                             		    
		    Endif
	   ENDIF    
    ENDIF
    
	ARQXML->(Dbskip())
End	                       
                    
fGrvLog("Finalizacao da Importacao de XML ")   

cEmpAnt := cEmpAux
cFilAnt := cFilAux 
fClose(nHdlLog)

Dbselectarea("ARQXML")                                            
DbClearFil() 

If lErro                              
    c_Para   := SuperGetMv("MV_XXXML",,"auditoria4@bagaggio.com.br;jonathas.rangel@bagaggio.com.br")
    cMailMsg := Memoread(cArqLog)   
    cArqEXEC := Substr(cArqLog,1,24)+".log"    
//	U_ZSendMail(,,,,c_Para,"LOG - Importacao XML", cMailMsg,cArqEXEC,,.F.)
Endif

If Select("ARQXML") > 0
   ARQXML->(DbCloseArea())
EndIf           

If c_Orig <> Nil
   Reset Environment
Endif
   
c_Ret := "("+Str(n_QtdNF,4)+") Notas Fiscais Importadas."	                         
Return(c_Ret)                     

**************************************
Static Function ZUPDNFE(c_MSG,n_opc)
**************************************    
           
Dbselectarea("ARQXML")
Reclock("ARQXML",.F.)                 
If n_Opc == 1
	ARQXML->IMPORTADO_ERP := "S"
	ARQXML->SITUACAO      := "Conciliada: "+Dtoc(dDataBase)+" - "+Time()
ElseIf n_Opc == 2                            
	ARQXML->IMPORTADO_ERP := "C"
	ARQXML->SITUACAO      := "Erro: "+Left(Alltrim(c_MSG),240)
	lErro       := .T.
ElseIf n_Opc == 3                            
	ARQXML->IMPORTADO_ERP := "S"
	ARQXML->SITUACAO      := "Erro: "+Left(Alltrim(c_MSG),240)
	lErro       := .T.	
Endif	
Msunlock()

Return(.T.)


*******************************
Static Function fGrvLog(cTexto)
*******************************
If nHdlLog == 0
	cArqLog := "\zeus\ProcXml\"+SubStr(DToS(Date()),3)+StrTran(SubStr(Time(),1,5),":")+".log"
	nHdlLog  := fCreate(cArqLog)
EndIf         
cTexto := DToC(Date())+" - "+Time()+" - "+cTexto
fWrite(nHdlLog,cTexto+Chr(13)+Chr(10))
Return                

Static Function F_ImpXml(cCodBar,c_FilLoja)
*******************************************		
Local c_XmlRet  := ""
Local nX        := 0
Local Ni        := 0
Private n_QtdPC := 0
Private n_QtdNF := 0    
Private _cMarca := GetMark()
Private c_Chave := ARQXML->CNPJ+ARQXML->NUMERO
                 
cAviso     := ""
cErro      := ""      
cWarning   := ""
l_Continua := .T.              
c_XmlRet   := U_ZNoAcento(AnsiToOem(AllTrim(cCodBar)),.T.)
oNfe       := XmlParser(c_XmlRet,"_",@cAviso,@cErro) 
//oNfe       := XmlParserFile( cCodBar, "_", @cAviso, @cErro )

Private oNF         

If Left(cAviso,19) == "Input is not proper"
	fGrvLog("XML Corrompido - Verifique " + c_Chave)
	ZUPDNFE("XML Corrompido - Verifique " + c_Chave,2)
	Return
Endif                                           

If Type("oNFe:_NfeProc")<> "U"
	oNF := oNFe:_NFeProc:_NFe
ElseIf Type("oNFe:_RETDOWNLOADNFE:_RETNFE:_PROCNFE:_NFEPROC")<> "U"	
	oNF := oNFe:_RETDOWNLOADNFE:_RETNFE:_PROCNFE:_NFEPROC:_NFe      
ElseIf "REJEICAO" $ Upper(Alltrim(oNfe:_RETDOWNLOADNFE:_RETNFE:_XMOTIVO:TEXT))
	l_Continua := .F.
Else
	oNF := oNFe:_NFe 		
Endif

fGrvLog("Processando XML, Filial ("+c_FilLoja+"): "+ALLTRIM(ARQXML->NUMERO)+" - "+ARQXML->CHAVE_NFE)                                                                       
If l_Continua                                                       

	Private oEmitente  := oNF:_InfNfe:_Emit
	Private oIdent     := oNF:_InfNfe:_IDE
	Private oDestino   := oNF:_InfNfe:_Dest
	Private oTotal     := oNF:_InfNfe:_Total
	Private oTransp    := oNF:_InfNfe:_Transp
	Private oDet       := oNF:_InfNfe:_Det      
	Private c_Chvnfe   := Right(Alltrim(onf:_INFNFE:_ID:TEXT),44)
	Private oICM       := IIF(Type("oNF:_InfNfe:_ICMS")<> "U",oNF:_InfNfe:_ICMS,Nil)	      
	/*
	If Type("oNF:_InfNfe:_ICMS")<> "U"
		Private oICM       := oNF:_InfNfe:_ICMS
	Else
		Private oICM		:= nil
	Endif
	*/
	Private oFatura    := IIf(Type("oNF:_InfNfe:_Cobr")=="U",Nil,oNF:_InfNfe:_Cobr)
	Private cEdit1	   := Space(30)
	Private _DESCdigit :=space(55)
	Private _NCMdigit  :=space(8)
			
	oDet := IIf(ValType(oDet)=="O",{oDet},oDet)
	// Valida��es -------------------------------
	// -- CNPJ da NOTA = CNPJ do CLIENTE ? oEmitente:_CNPJ
	cTipo := "N"
			
	// CNPJ ou CPF
	cCNPJInf := oDestino:_CNPJ:TEXT 		          
    cFilAnt := c_FilLoja
	
	cCgc := AllTrim(IIf(Type("oEmitente:_CPF")=="U",oEmitente:_CNPJ:TEXT,oEmitente:_CPF:TEXT))	
	If !SA2->(dbSetOrder(3), dbSeek(xFilial("SA2")+cCgc))
		fGrvLog("CNPJ Origem N�o Localizado - Verifique " + cCgc)
		ZUPDNFE("CNPJ Origem N�o Localizado - " + cCgc,2)
		Return
	Endif                                                                                                           	                                                                                                          	
	
	// -- Nota Fiscal j� existe na base ?     
	If SF1->(DbSeek(cFilAnt+Right("000000000"+Alltrim(OIdent:_nNF:TEXT),9)+Padr(OIdent:_serie:TEXT,3)+SA2->A2_COD+SA2->A2_LOJA))
		fGrvLog("Nota No.: "+Right("000000000"+Alltrim(OIdent:_nNF:TEXT),9)+"/"+OIdent:_serie:TEXT+" do Fornec. "+SA2->A2_COD+"/"+SA2->A2_LOJA+" Ja Existe.")
		ZUPDNFE('',1) // Marcar como ja importado
		Return
	EndIf                                                                                                           
	
	If ONF:_INFNFE:_IDE:_FINNFE:TEXT == "4" //Devolucao
		//fGrvLog("NFe de Devolucao - Entrar Manualmente !!" + c_Chave)
		ZUPDNFE("NFe de Devolucao - Entrar Manualmente - " + c_Chave,3)
		Return
	Endif 
				
	aCabec := {}
	aItens := {}
	aadd(aCabec,{"F1_TIPO"   ,"N",Nil,Nil})
	aadd(aCabec,{"F1_FORMUL" ,"N",Nil,Nil})
	aadd(aCabec,{"F1_DOC"    ,Right("000000000"+Alltrim(OIdent:_nNF:TEXT),9),Nil,Nil})
	aadd(aCabec,{"F1_SERIE"  ,OIdent:_serie:TEXT,Nil,Nil})
	              
	If oNF:_INFNFE:_VERSAO:TEXT >= "3.10"
		cData:=Alltrim(OIdent:_dHEmi:TEXT)
		dData:=CTOD(Substr(cData,9,2)+'/'+Substr(cData,6,2)+'/'+Substr(cData,1,4))
	Else 
		cData:=Alltrim(OIdent:_dEmi:TEXT)
		dData:=CTOD(Right(cData,2)+'/'+Substr(cData,6,2)+'/'+Left(cData,4))	
	Endif		
	aadd(aCabec,{"F1_EMISSAO",dData,Nil,Nil})
	aadd(aCabec,{"F1_FORNECE",SA2->A2_COD,Nil,Nil})
	aadd(aCabec,{"F1_LOJA"   ,SA2->A2_LOJA,Nil,Nil})
	aadd(aCabec,{"F1_ESPECIE","SPED",Nil,Nil})
	aadd(aCabec,{"F1_CHVNFE" ,c_Chvnfe,Nil,Nil})
	
	If cTipo == "N"
		aadd(aCabec,{"F1_COND" ,If(Empty(SA2->A2_COND),'004',SA2->A2_COND),Nil,Nil})
	Else
		aadd(aCabec,{"F1_COND" ,If(Empty(SA1->A1_COND),'004',SA1->A1_COND),Nil,Nil})
	Endif
		
	// Primeiro Processamento
	// Busca de Informa��es para Pedidos de Compras
	
	cProds := ''
	aPedIte:={}
	
	For nX := 1 To Len(oDet)
		cEdit1     := Space(30)
		_DESCdigit :=space(55)
		_NCMdigit  :=space(8)
		
		cProduto:= PadR(AllTrim(oDet[nX]:_Prod:_cProd:TEXT),30)
		cEanFor := PadR(AllTrim(oDet[nX]:_Prod:_cEan:TEXT),30)
		xProduto:= cProduto
		lVilla  := .F.   
		cNCM    := IIF(Type("oDet[nX]:_Prod:_NCM")=="U",space(12),oDet[nX]:_Prod:_NCM:TEXT)
		Chkproc := .F.

		If SA2->A2_COD $ "000022|100000|200000"
			cEdit1 := cProduto                     
	        lVilla := .T. 
	        Chkproc:= .T.	
		Endif
				
		SA5->(DbSetOrder(14))   // FILIAL + FORNECEDOR + LOJA + CODIGO PRODUTO NO FORNECEDOR
		If !SA5->(dbSeek(xFilial("SA5")+SA2->A2_COD+SA2->A2_LOJA+Substr(cProduto,1,20)))
			IF !lVilla
				fGrvLog("Produto Cod.: "+Alltrim(cProduto)+" Nao Encontrado. Fornecedor : "+SA2->A2_COD+"/"+SA2->A2_LOJA+" - "+SA2->A2_NOME)				
				ZUPDNFE("Produto Cod.: "+Alltrim(cProduto)+" Nao Encontrado. Fornecedor : "+SA2->A2_COD+"/"+SA2->A2_LOJA+" - "+SA2->A2_NOME,2)
				Return
			Else
				SB1->(Dbsetorder(1))
				SB1->(Dbseek(xFilial("SB1")+cEdit1))

			    SA5->(dbSetOrder(14))
			 	If !SA5->(dbSeek(xFilial("SA5")+SA2->A2_COD+SA2->A2_LOJA+xProduto))
					//RecLock("SA5",.f.)
					//Else
					Reclock("SA5",.T.)
					SA5->A5_FILIAL := xFilial("SA5")
					SA5->A5_FORNECE := SA2->A2_COD
					SA5->A5_LOJA 	:= SA2->A2_LOJA
					SA5->A5_NOMEFOR := SA2->A2_NOME
					SA5->A5_PRODUTO := SB1->B1_COD	
					SA5->A5_NOMPROD := oDet[nX]:_Prod:_xProd:TEXT
					SA5->A5_CODPRF  := xProduto
					SA5->(MsUnlock())										
				Endif					
			EndIf
		Else
			SB1->(dbSetOrder(1))
			If SB1->(dbSeek(xFilial("SB1")+SA5->A5_PRODUTO))				
				If !Empty(cNCM) .and. cNCM != '00000000' .And. SB1->B1_POSIPI <> cNCM .And. !lVilla
					RecLock("SB1",.F.)
					Replace B1_POSIPI with cNCM
					MSUnLock()
				Endif
			Else
			    fGrvLog("Fornecedor "+SA5->A5_CODPRF+" - Produto "+SA5->A5_PRODUTO+" n�o Encontrado no CAD.PRODUTO. A Importacao sera interrompida")
			    ZUPDNFE("Fornecedor "+SA5->A5_CODPRF+" - Produto "+SA5->A5_PRODUTO+" n�o Encontrado no CAD.PRODUTO.",2)
			    Return	
			Endif	
		Endif           
			
		SB1->(dbSetOrder(1))
		
		cProds += ALLTRIM(SB1->B1_COD)+'/'
		
		AAdd(aPedIte,{SB1->B1_COD,Val(oDet[nX]:_Prod:_qTrib:TEXT),Round(Val(oDet[nX]:_Prod:_vProd:TEXT)/Val(oDet[nX]:_Prod:_qCom:TEXT),6),Val(oDet[nX]:_Prod:_vProd:TEXT)})
		
	Next nX
	                                               	
	// Retira a Ultima "/" da Variavel cProds
	
	cProds := Left(cProds,Len(cProds)-1)
	
	aCampos := {}
	aCampos2:= {}
	
	AADD(aCampos,{'T9_OK'			,'#','@!','2','0'})
	AADD(aCampos,{'T9_PEDIDO'		,'Pedido','@!','6','0'})
	AADD(aCampos,{'T9_ITEM'			,'Item','@!','3','0'})
	AADD(aCampos,{'T9_PRODUTO'		,'PRODUTO','@!','15','0'})
	AADD(aCampos,{'T9_DESC'			,'Descri��o','@!','40','0'})
	AADD(aCampos,{'T9_UM'			,'Un','@!','02','0'})
	AADD(aCampos,{'T9_QTDE'			,'Qtde','@EZ 999,999.9999','10','4'})
	AADD(aCampos,{'T9_UNIT'			,'Unitario','@EZ 9,999,999.99','12','2'})
	AADD(aCampos,{'T9_TOTAL'		,'Total','@EZ 99,999,999.99','14','2'})
	AADD(aCampos,{'T9_DTPRV'		,'Dt.Prev','','10','0'})
	AADD(aCampos,{'T9_ALMOX'		,'Alm','','2','0'})
	AADD(aCampos,{'T9_OBSERV'		,'Observa��o','@!','30','0'})
	AADD(aCampos,{'T9_CCUSTO'		,'C.Custo','@!','6','0'})   
	AADD(aCampos,{'T9_GRADE'		,'Grade' ,'@!','01','0'})
	AADD(aCampos,{'T9_ITEMGRD'		,'Item GRD','@!','03','0'})	
	
	AADD(aCampos2,{'T8_NOTA'		,'N.Fiscal','@!','9','0'})
	AADD(aCampos2,{'T8_SERIE'		,'Serie','@!','3','0'})
	AADD(aCampos2,{'T8_PRODUTO'		,'PRODUTO','@!','15','0'})
	AADD(aCampos2,{'T8_DESC'		,'Descri��o','@!','40','0'})
	AADD(aCampos2,{'T8_UM'			,'Un','@!','02','0'})
	AADD(aCampos2,{'T8_QTDE'		,'Qtde','@EZ 9,999.9999','10','4'})
	AADD(aCampos2,{'T8_UNIT'		,'Unitario','@EZ 99,999.99','10','2'})
	AADD(aCampos2,{'T8_TOTAL'		,'Total','@EZ 999,999.99','12','2'})
	AADD(aCampos2,{'T8_PEDIDO'		,'Pedido' ,'@!','06','0'})
	AADD(aCampos2,{'T8_ITEMPC'		,'Item PC','@!','04','0'})
	AADD(aCampos2,{'T8_GRADE'		,'Grade' ,'@!','01','0'})
	AADD(aCampos2,{'T8_ITEMGRD'		,'Item GRD','@!','03','0'})
	
	Cria_TC9()
	
	For ni := 1 To Len(aPedIte)
		RecLock("TC8",.t.)
		TC8->T8_NOTA 	:= Right("000000000"+Alltrim(OIdent:_nNF:TEXT),9)
		TC8->T8_SERIE 	:= OIdent:_serie:TEXT
		TC8->T8_PRODUTO := aPedIte[nI,1]
		TC8->T8_DESC	:= Posicione("SB1",1,xFilial("SB1")+aPedIte[nI,1],"B1_DESC")
		TC8->T8_UM		:= SB1->B1_UM   
		TC8->T8_QTDE	:= aPedIte[nI,2]
		TC8->T8_UNIT	:= aPedIte[nI,3]
		TC8->T8_TOTAL	:= aPedIte[nI,4]
		TC8->(msUnlock()) 
		n_QtdNF += 1
	Next
	TC8->(dbGoTop())
	
	If c_Ped <> Nil
		lOk := .F.			                                     
		fGrvLog("Nao usou o PC!!")
	Else 
		Monta_TC9()	
		TC8->(dbGoTop())		    
		lOk := .T.		
		If Empty(TC9->(RecCount()))      					
			lOk := .F.		
		Endif
	Endif	
	
	IF lOk                                                           	
		// Verifica se o usuario selecionou algum pedido de compra	
		dbSelectArea("TC9")
		dbGoTop()
		ProcRegua(Reccount())
		
		lMarcou := .f.
		
		While !Eof() .And. lOk
			IncProc()
			If TC9->T9_OK  <> _cMarca
				dbSelectArea("TC9")
				TC9->(dbSkip(1));Loop
			Else
				lMarcou := .T.
				Exit
			Endif
			
			TC9->(dbSkip(1))
		Enddo			
	Endif                                          
			
	For nX := 1 To Len(oDet)
				
		aLinha := {}
		cProduto:=PadR(AllTrim(oDet[nX]:_Prod:_cProd:TEXT),30)
		cEanFor :=PadR(AllTrim(oDet[nX]:_Prod:_cEan:TEXT),30)
		xProduto:=cProduto
		
		cNCM:=IIF(Type("oDet[nX]:_Prod:_NCM")=="U",space(12),oDet[nX]:_Prod:_NCM:TEXT)
		
		SA5->(DbSetOrder(14))   // FILIAL + FORNECEDOR + LOJA + CODIGO PRODUTO NO FORNECEDOR
		SA5->(dbSeek(xFilial("SA5")+SA2->A2_COD+SA2->A2_LOJA+Substr(cProduto,1,20)))
		SB1->(dbSetOrder(1)) ; SB1->(dbSeek(xFilial("SB1")+SA5->A5_PRODUTO))
				
		aadd(aLinha,{"D1_ITEM",Strzero(nX,4),Nil,Nil}) 
		aadd(aLinha,{"D1_COD",SB1->B1_COD,Nil,Nil}) 
		If Val(oDet[nX]:_Prod:_qTrib:TEXT) != 0
			aadd(aLinha,{"D1_QUANT",Val(oDet[nX]:_Prod:_qTrib:TEXT),Nil,Nil})
			aadd(aLinha,{"D1_VUNIT",Round(Val(oDet[nX]:_Prod:_vProd:TEXT)/Val(oDet[nX]:_Prod:_qTrib:TEXT),6),Nil,Nil})
		Else
			aadd(aLinha,{"D1_QUANT",Val(oDet[nX]:_Prod:_qCom:TEXT),Nil,Nil})
			aadd(aLinha,{"D1_VUNIT",Round(Val(oDet[nX]:_Prod:_vProd:TEXT)/Val(oDet[nX]:_Prod:_qCom:TEXT),6),Nil,Nil})
		Endif
		
		aadd(aLinha,{"D1_TOTAL",Val(oDet[nX]:_Prod:_vProd:TEXT),Nil,Nil})
		_cfop:=oDet[nX]:_Prod:_CFOP:TEXT
		If Left(Alltrim(_cfop),1)="5"
			_cfop:=Stuff(_cfop,1,1,"1")
		Else
			_cfop:=Stuff(_cfop,1,1,"2")
		Endif       
		
		c_Tes := ""                     
		SBZ->(Dbsetorder(1))                                             
		If SBZ->(DbSeek(cFilAnt+SB1->B1_COD))
		   If !Empty(SBZ->BZ_TE)
			  c_Tes := SBZ->BZ_TE
		   Endif
		Endif
		
		If Empty(c_Tes)                                                   
		  If !Empty(SB1->B1_TE)
		     c_Tes := SB1->B1_TE
		  Endif    
		Endif                                   
		                         
		If !(SA2->A2_COD $ '000022|100000|200000') // Villarejo
		    aHeader := {}
			c_Tes1 := MaTesInt(1,"51",SA2->A2_COD,SA2->A2_LOJA,"F",SB1->B1_COD,NIL)
			If !Empty(c_Tes1) 
			   c_Tes := c_Tes1
			Endif
		Else	            
			aHeader := {}
			If Empty(c_Tes) 
				c_Tes := MaTesInt(1,"51",SA2->A2_COD,SA2->A2_LOJA,"F",SB1->B1_COD,NIL)
			Endif
		Endif
							
		aadd(aLinha,{"D1_01DESCR",cProduto,Nil,Nil})
		aadd(aLinha,{"D1_TES",c_Tes,Nil,Nil})
	//	aadd(aLinha,{"D1_CF",_cfop,Nil,Nil})
		If Type("oDet[nX]:_Prod:_vDesc")<> "U"
			aadd(aLinha,{"D1_VALDESC",Val(oDet[nX]:_Prod:_vDesc:TEXT),Nil,Nil})
		Endif
		Do Case
			Case Type("oDet[nX]:_Imposto:_ICMS:_ICMS00")<> "U"
				oICM:=oDet[nX]:_Imposto:_ICMS:_ICMS00
			Case Type("oDet[nX]:_Imposto:_ICMS:_ICMS10")<> "U"
				oICM:=oDet[nX]:_Imposto:_ICMS:_ICMS10
			Case Type("oDet[nX]:_Imposto:_ICMS:_ICMS20")<> "U"
				oICM:=oDet[nX]:_Imposto:_ICMS:_ICMS20
			Case Type("oDet[nX]:_Imposto:_ICMS:_ICMS30")<> "U"
				oICM:=oDet[nX]:_Imposto:_ICMS:_ICMS30
			Case Type("oDet[nX]:_Imposto:_ICMS:_ICMS40")<> "U"
				oICM:=oDet[nX]:_Imposto:_ICMS:_ICMS40
			Case Type("oDet[nX]:_Imposto:_ICMS:_ICMS51")<> "U"
				oICM:=oDet[nX]:_Imposto:_ICMS:_ICMS51
			Case Type("oDet[nX]:_Imposto:_ICMS:_ICMS60")<> "U"
				oICM:=oDet[nX]:_Imposto:_ICMS:_ICMS60
			Case Type("oDet[nX]:_Imposto:_ICMS:_ICMS70")<> "U"
				oICM:=oDet[nX]:_Imposto:_ICMS:_ICMS70
			Case Type("oDet[nX]:_Imposto:_ICMS:_ICMS90")<> "U"
				oICM:=oDet[nX]:_Imposto:_ICMS:_ICMS90
		EndCase
		If Type("oICM:_orig:TEXT") <> "U" .And. Type("oICM:_CST:TEXT") <> "U"
		   CST_Aux:=Alltrim(oICM:_orig:TEXT)+Alltrim(oICM:_CST:TEXT)
		   aadd(aLinha,{"D1_CLASFIS",CST_Aux,Nil,Nil})
		EndIf
		  
		TC8->(Dbsetorder(1))
		If TC8->(Dbseek(SB1->B1_COD)) 
		   If !Empty(TC8->T8_PEDIDO)
			   //	c_Numped  := CriaVar("D1_PEDIDO")
				//  c_ItemPed := CriaVar("D1_ITEMPC")
				aadd(aLinha,{"D1_PEDIDO",TC8->T8_PEDIDO ,Nil,Nil})
				aadd(aLinha,{"D1_ITEMPC",TC8->T8_ITEMPC ,Nil,Nil})   
				aadd(aLinha,{"D1_GRADE"  ,TC8->T8_GRADE  ,Nil,Nil})
				aadd(aLinha,{"D1_ITEMGRD",TC8->T8_ITEMGRD,Nil,Nil})				
		   Endif	
		Endif		
		
		aadd(aItens,aLinha)
	Next nX                                                                     
		
	TC8->(dbCloseArea())
	TC9->(dbCloseArea())
	
	//��������������������������������������������������������������Ŀ
	//| Teste de Inclusao                                            |
	//����������������������������������������������������������������

	If Len(aItens) > 0
		Private lMsErroAuto := .f.
		Private lMsHelpAuto := .T.
		
		SB1->( dbSetOrder(1) )
		SA2->( dbSetOrder(1) )
		
	//	nModulo := 4  //ESTOQUE
        MSExecAuto({|x,y,z|Mata140(x,y,z)},aCabec,aItens,3)
		
		IF lMsErroAuto						
			fGrvLog("NFe : "+Alltrim(aCabec[3,2])+" / "+Alltrim(aCabec[4,2])+" com ERROR.LOG, entrar Manualmente !!")
	        Ferase("\zeus\ProcXml\Autoexec\"+c_Chave+".TXT")
		    MostraErro("\zeus\ProcXml\Autoexec",c_Chave+".TXT")
		    c_Msg1 := MemoRead("\zeus\ProcXml\Autoexec\"+c_Chave+".TXT")
			ZUPDNFE(c_Msg1,2) 
		Else
			ZUPDNFE('',1) // Marcar como ja importado
			fGrvLog(Alltrim(aCabec[3,2])+' / '+Alltrim(aCabec[4,2])+" - Pr� Nota Gerada Com Sucesso!")			
		EndIf
	Endif
Else
	fGrvLog("Erro na geracao do Objeto NFe : "+c_Chave)   	
Endif
	
Return
/*
Static Function F_MoveArq(c_Arquivo,c_Caminho,n_Opc)
**************************************************** 
If n_Opc == 1
	xFile := STRTRAN(c_Arquivo,c_Caminho, "C:\TOTVS11\SYSTEM\XMLNFE\REJEITADAS\")				
Else  
	xFile := STRTRAN(c_Arquivo,c_Caminho, "C:\TOTVS11\SYSTEM\XMLNFE\PROCESSADAS\")
Endif
	
COPY FILE &c_Arquivo TO &xFile				
FErase(c_Arquivo)
fGrvLog("Arquivo "+c_Arquivo+" Movido para a PASTA "+xFile)
Return(.T.)  	

Static Function C(nTam)
***********************
Local nHRes	:=	oMainWnd:nClientWidth	// Resolucao horizontal do monitor
If nHRes == 640	// Resolucao 640x480 (soh o Ocean e o Classic aceitam 640)
	nTam *= 0.8
ElseIf (nHRes == 798).Or.(nHRes == 800)	// Resolucao 800x600
	nTam *= 1
Else	// Resolucao 1024x768 e acima
	nTam *= 1.28
EndIf

//���������������������������Ŀ
//�Tratamento para tema "Flat"�
//�����������������������������
If "MP8" $ oApp:cVersion
	If (Alltrim(GetTheme()) == "FLAT") .Or. SetMdiChild()
		nTam *= 0.90
	EndIf
EndIf
Return Int(nTam)
*/
******************************************************
Static FUNCTION Cria_TC9()

If Select("TC9") <> 0
	TC9->(dbCloseArea())
Endif
If Select("TC8") <> 0
	TC8->(dbCloseArea())     	
Endif

aFields   := {}
AADD(aFields,{"T9_OK"     ,"C",02,0})
AADD(aFields,{"T9_PEDIDO" ,"C",06,0})
AADD(aFields,{"T9_ITEM"   ,"C",04,0})
AADD(aFields,{"T9_PRODUTO","C",15,0})
AADD(aFields,{"T9_DESC"   ,"C",40,0})
AADD(aFields,{"T9_UM"     ,"C",02,0})
AADD(aFields,{"T9_QTDE"   ,"N",6,0})
AADD(aFields,{"T9_UNIT"   ,"N",12,2})
AADD(aFields,{"T9_TOTAL"  ,"N",14,2})
AADD(aFields,{"T9_DTPRV"  ,"D",08,0})
AADD(aFields,{"T9_ALMOX"  ,"C",02,0})
AADD(aFields,{"T9_OBSERV" ,"C",30,0})
AADD(aFields,{"T9_CCUSTO" ,"C",06,0})
AADD(aFields,{"T9_GRADE"  ,"C",01,0})
AADD(aFields,{"T9_ITEMGRD","C",03,0})
AADD(aFields,{"T9_REG" ,"N",10,0})
cArq:=Criatrab(aFields,.T.)
DBUSEAREA(.t.,,cArq,"TC9")   
IndRegua("TC9",cArq,"T9_PEDIDO",,,"Selecionando Registros...")

aFields2   := {}
AADD(aFields2,{"T8_NOTA"   ,"C",09,0})
AADD(aFields2,{"T8_SERIE"  ,"C",03,0})
AADD(aFields2,{"T8_PRODUTO","C",15,0})
AADD(aFields2,{"T8_DESC"   ,"C",40,0})
AADD(aFields2,{"T8_UM"     ,"C",02,0})
AADD(aFields2,{"T8_QTDE"   ,"N",6,0})
AADD(aFields2,{"T8_UNIT"   ,"N",12,2})
AADD(aFields2,{"T8_TOTAL"  ,"N",14,2})
AADD(aFields2,{"T8_PEDIDO" ,"C",06,0})
AADD(aFields2,{"T8_ITEMPC" ,"C",04,0})   
AADD(aFields2,{"T8_GRADE"  ,"C",01,0})
AADD(aFields2,{"T8_ITEMGRD","C",03,0})

cArq2:=Criatrab(aFields2,.T.)
DBUSEAREA(.t.,,cArq2,"TC8")
IndRegua("TC8",cArq2,"T8_PRODUTO",,,"Selecionando Registros...")

Return


********************************************
Static Function Monta_TC9()
********************************************
Local _nX := 0

// Ir� efetuar a checagem de pedidos de compras
// em aberto para este fornecedor e os itens desta nota fiscal a ser importa
// ser� demonstrado ao usu�rio se o pedido de compra dever� ser associado
// a entrada desta nota fiscal

cQuery := ""
cQuery += " SELECT  C7_NUM T9_PEDIDO,     "
cQuery += " 		C7_ITEM T9_ITEM,    "
cQuery += " 	    C7_PRODUTO T9_PRODUTO, "
cQuery += " 		B1_DESC T9_DESC,    "
cQuery += " 		B1_UM T9_UM,		"
cQuery += " 		C7_QUANT T9_QTDE,   "
cQuery += " 		C7_PRECO T9_UNIT,   "
cQuery += " 		C7_TOTAL T9_TOTAL,   "
cQuery += " 		C7_DATPRF T9_DTPRV,  "
cQuery += " 		C7_LOCAL T9_ALMOX, "
cQuery += " 		C7_OBS T9_OBSERV, "
cQuery += " 		C7_CC T9_CCUSTO, "  
cQuery += " 		C7_GRADE T9_GRADE, "
cQuery += " 		C7_ITEMGRD T9_ITEMGRD, "
cQuery += " 		SC7.R_E_C_N_O_ T9_REG "
cQuery += " FROM " + RetSqlName("SC7") + " SC7, " + RetSqlName("SB1") + " SB1 "
cQuery += " WHERE C7_FILIAL = '" + cFilAnt + "' "
cQuery += " AND B1_FILIAL = '" + xFilial("SB1") + "' "
cQuery += " AND SC7.D_E_L_E_T_ = ' ' "
cQuery += " AND SB1.D_E_L_E_T_ = ' ' "
cQuery += " AND C7_QUANT > C7_QUJE  "
cQuery += " AND C7_RESIDUO = ''  "
//	cQuery += " AND C7_TPOP <> 'P'  "
cQuery += " AND C7_CONAPRO <> 'B'  "
cQuery += " AND C7_ENCER = '' "
//	cQuery += " AND C7_CONTRA = '' "
//	cQuery += " AND C7_MEDICAO = '' "
cQuery += " AND C7_PRODUTO = B1_COD "
cQuery += " AND C7_FORNECE = '" + SA2->A2_COD + "' "
cQuery += " AND C7_LOJA = '" + SA2->A2_LOJA + "' "
cQuery += " AND C7_PRODUTO IN" + FormatIn( cProds, "/")
//cQuery += " ORDER BY C7_PRODUTO "
cQuery += " ORDER BY C7_PRODUTO, SC7.R_E_C_N_O_ DESC "
cQuery := ChangeQuery(cQuery)
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"CAD",.T.,.T.)
TcSetField("CAD","T9_DTPRV","D",8,0)

Dbselectarea("CAD")

While CAD->(!EOF())
	RecLock("TC9",.T.)
	For _nX := 1 To Len(aFields)
		If !(aFields[_nX,1] $ 'T9_OK')
			If aFields[_nX,2] = 'C'
				_cX := 'TC9->'+aFields[_nX,1]+' := Alltrim(CAD->'+aFields[_nX,1]+')'
			Else
				_cX := 'TC9->'+aFields[_nX,1]+' := CAD->'+aFields[_nX,1]
			Endif
			_cX := &_cX
		Endif
	Next
	TC9->T9_OK := _cMarca //ThisMark()
	MsUnLock()                                                       
	
	n_QtdPC += 1        
	
	TC8->(Dbsetorder(1))
	If TC8->(Dbseek(TC9->T9_PRODUTO))
		RecLock("TC8",.F.)	
		   TC8->T8_PEDIDO  := TC9->T9_PEDIDO 
		   TC8->T8_ITEMPC  := TC9->T9_ITEM      
		   TC8->T8_GRADE   := TC9->T9_GRADE 
		   TC8->T8_ITEMGRD := TC9->T9_ITEMGRD		   
	    Msunlock()
	Endif
	                      	
	DbSelectArea('CAD')
	CAD->(dBSkip())
EndDo                     

Dbselectarea("CAD")
DbCloseArea()
Dbselectarea("TC9")
DbGoTop()

Return