/*#########################################################################################*/
/*# Programa : zBAF001              Autor : Marcio Santos              Data : 08/10/2013  #*/
/*#########################################################################################*/
/*# Descricao : Gerar pedido dos produtos com saldo em estoque.                           #*/
/*#########################################################################################*/

#Include 'Protheus.ch'
#Include "rwmake.ch"
#INCLUDE "TOPCONN.CH"   


/*************************/
User Function ZBAGF001()     
/*************************/           
Private Adial

Define Font oFont Name "Tahoma" Bold
Define MsDialog aDial Title OemToAnsi(" Gera Pedido de Venda ") From 000,000 To 330,430 Of aDial Pixel

@ 005,005 To 030,210 Title ""
//            0         1         2         3         4         5         6      |Limite   
//            01234567890123456789012345678901234567890123456789012345678901234567890
@ 010,010 Say "  Este programa tem por objetivo gerar pedido com as mercadorias  " Font oFont Color CLR_BLACK Pixel
@ 020,010 Say "  com saldo em estoque.                                           " Font oFont Color CLR_BLACK Pixel

@ 035,005 To 145,210 Title "" 
@ 055,150 BmpButton Type 01 Action fGerPed1()
@ 055,180 BmpButton Type 02 Action aDial:End()

Activate Dialog aDial Centered  

RETURN

Static Function fGerPed1()
**************************   
Private c_Cli  := Space(06)
Private c_Loja := Space(02) 
Private c_Tes  := Space(03)

If !Pergunte("ZBAGF001  ",.T.)
   Return(.T.)         
Else         
   If MV_PAR01 == 1
	   c_Cli  := "100000" 
   Else	   
	   c_Cli  := "200000" 
   Endif	   
   c_Loja := MV_PAR02
   c_TES  := MV_PAR03
Endif
 
DbSelectArea("SB1")
DbSetOrder(1)               

SA1->(Dbsetorder(1))
SA1->(Dbseek(xfilial("SA1")+c_Cli+c_Loja))  

Private lMsErroAuto := .F., lMsHelpAuto := .T., lAutoErrNoFile := .F.

cMenNota := "PRODUTOS EM ESTOQUE"    
cMenPad  := CriaVar("C5_MENPAD")

aCabPed := {{'C5_TIPO'    , 'N'        ,Nil},;               //2- Tipo de pedido
			{'C5_CLIENTE' , c_Cli      ,Nil},;               //3- Codigo do cliente
			{'C5_LOJACLI' , c_Loja     ,Nil},;               //5- Loja do cliente
            {"C5_TIPOCLI" , SA1->A1_TIPO,Nil},;
            {"C5_TABELA"  , '004'      ,Nil},;
			{'C5_CONDPAG' , '001'	   ,Nil},;                    //7- Condicao de pagamanto
			{'C5_EMISSAO' , dDataBase  ,Nil},;               //8- Data de emissao
            {"C5_TPFRETE" , "C"        ,Nil},;
            {"C5_MENPAD"  , cMenPad    ,Nil},;
			{'C5_MENNOTA' , cMenNota   ,Nil}}             

If Select('QRY1') > 0
	QRY1->(DbCloseArea())
EndIf

cQUERY := "SELECT B2_COD, B2_LOCAL, B2_QATU, B2_CM1			   "
cQUERY += "FROM       "+RetSqlName("SB2")+ " SB2               "
cQUERY += "INNER JOIN "+RetSqlName("SB1")+ " SB1 ON B2_COD = B1_COD "
cQUERY += "WHERE B2_FILIAL = '"+xFilial("SB2")+"'              "
cQUERY += "  AND B2_QATU > 0 AND B2_CM1 > 0 AND B2_LOCAL = '01' "  
cQUERY += "  AND SB2.D_E_L_E_T_ = ' '                          "
cQUERY += "  AND SB1.D_E_L_E_T_ = ' '                          "

cQUERY := ChangeQuery(cQuery)
DbUseArea(.T., 'TOPCONN', TcGenQry(,, cQuery), 'QRY1', .T., .T.)

DbSelectArea("QRY1")
QRY1->(DbGoTop())
nRegs := Contar("QRY1","! Eof()")
QRY1->(DbGoTop())

n_Item    := 0  
cItem     := '00'
aItensPed := {}

While !QRY1->(Eof())                                                                                                           
        
		SA1->(Dbsetorder(1))
		SA1->(Dbseek(xfilial("SA1")+c_Cli+c_Loja))  
        
   		SB1->(Dbsetorder(1))
		SB1->(Dbseek(xFilial("SB1")+QRY1->B2_COD))
	                           
	    If SB1->B1_MSBLQL <> '1'
	         	                                            
			cTesInt := MaTesInt(2,"01",SA1->A1_COD,SA1->A1_LOJA,"C",QRY1->B2_COD,NIL)		
			If !Empty(cTesInt)
				cTes := cTesInt
			Else
				cTes := If( Empty( RetFldProd( SB1->B1_COD,"B1_TS" ) ), '521', RetFldProd( SB1->B1_COD,"B1_TS" ) )
			Endif   

	        If c_Cli == '100000' 	    				
				cTes := If(!Empty(c_TES),c_TES,'523')						
			Else 
			    If cTes == "501"
			       cTes := "528"
			    Else 
			       cTes := "573"			    
			    Endif   
			Endif	
	                 
			cRefGrade  := Left(Alltrim(QRY1->B2_COD),7)+Space(26-Len(Left(Alltrim(QRY1->B2_COD),7)))

	        DA1->(DbSetOrder(6))                        
	        DA1->(DbSeek(Left(cFilAnt,4)+'  '+cRefGrade+'004'))		
	        nPrcVen    := If(DA1->(!Eof()) , DA1->DA1_PRCVEN , 0)
			                 
			If nPrcVen <= 0
			    nPrcVen := QRY1->B2_CM1
		    Endif
		        
	        nVlrTot    := Round((nPrcVen * QRY1->B2_QATU),2)
	              
	        If nPrcVen > 0
				cItem := Soma1(cItem)
				aAdd(aItensPed, {{'C6_PRODUTO'  , QRY1->B2_COD          , Nil},;       //3- Codigo do Produto.
						 		 {'C6_QTDVEN'   , QRY1->B2_QATU         , Nil},;       //4- Quantidade transferencia [Un].
		                         {"C6_PRCVEN"   , nPrcVen  			   , Nil},; 
		                         {"C6_VALOR"    , nVlrTot  			   , Nil},; 
						         {'C6_LOCAL'    , QRY1->B2_LOCAL        , Nil},;       //5- Almoxarifado.
						 		 {'C6_TES'      , cTes                 , Nil},;       //6- TES.
		                         {"C6_PRUNIT"   , nPrcVen              , Nil},; 
		                         {"C6_QTDLIB"   , QRY1->B2_QATU         , Nil},;
		                         {"C6_ITEM"     , cItem                , Nil},;
		                         {"C6_ENTREG"   , dDataBase            , Nil}})				 		 
				
				If n_Item == 98
					fExecAuto()                               			
					n_Item    := 0  
					cItem     := '00'
					aItensPed := {}	                  
				//	Exit
				Else
				    n_Item += 1	
				EndIf		  
			Endif 
		Endif		 		                  		
	QRY1->(DbSkip())
End

If n_Item > 0
   fExecAuto()                               			
EndIf		   

MsgBox("Pedidos inclu�dos com sucesso! ")

Return

/*---------------------------------------------------------------------------------------------------------------*/

Static Function fExecAuto() 

If (Len(aItensPed) > 0)
	lMsErroAuto := .F.
	
	Begin Transaction 
		Processa({|lEnd| MsExecAuto({|x,y,z|Mata410(x,y,z)}, aCabPed, aItensPed, 3) },'Gerando pedido...', 'Aguarde...', .T.)			
		If lMsErroAuto
   			MsgBox('Erro ao incluir os Pedidos de Vendas...')   	    
			MostraErro()
			DisarmTransaction()
		End
	End Transaction
    
Else
	MsgBox('N�o h� itens associados ao Pedido...')   
EndIf

// Reseta vari�veis do corpo do Pedido
aItensPed := {}
cItem := '00' 

Return